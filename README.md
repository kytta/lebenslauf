lebenslauf
==========

> LaTeX template for clean and beautiful CVs and résumés

> **THIS IS A WORK IN PROGRESS.** 

Usage
-----

1. Put the `lebenslauf.cls` in the same directory you put your `.tex` files to
2. Use the `\documentclass` directive:

   ```tex
   \documentclass{lebenslauf}
   ```

Licence
-------

[BSD-3-Clause](https://spdx.org/licenses/BSD-3-Clause.html) © 2021, Nikita Karamov

----

The source code is made available
on [GitHub](https://github.com/NickKaramoff/lebenslauf),
on [GitLab](https://gitlab.com/NickKaramoff/lebenslauf),
and on [Codeberg](https://codeberg.org/NickKaramoff/lebenslauf).
